﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace DnsClass
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.OutputEncoding = Encoding.UTF8;
                // Lấy tên máy cục bộ
                Console.Title = "Dns Class";
                string hostName = Dns.GetHostName();
                Console.WriteLine($"Local host name: {hostName}");
                // Lấy các địa chỉ Ip mà tên miền google.com trỏ tới
                Console.Write("Nhập vào domain: ");
                string domain = Console.ReadLine();
                if (domain == "exit") break;
                IPAddress[] addresses = Dns.GetHostAddresses(domain);
                Console.Write("Address: ");
                foreach (var item in addresses)
                {
                    Console.WriteLine(item);
                }
                // lấy tất cả thông tin về google.com
                IPHostEntry entry = Dns.GetHostEntry(domain);
                Console.WriteLine($"HostEntry of {domain}");
                Console.WriteLine($"Host name: {entry.HostName}");
                Console.Write("Address: ");
                foreach (IPAddress ip in entry.AddressList)
                {
                    Console.WriteLine(ip);
                }
                Console.WriteLine("Aliases: ");
                foreach (string alias in entry.Aliases)
                {
                    Console.WriteLine(alias);
                }
                Console.WriteLine("==================================");
            }
        }
    }
}
